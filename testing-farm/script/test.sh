#!/bin/bash -e

set +x

[ -z "$SCRIPT" ] && SCRIPT="cat /etc/os-release"

eval $SCRIPT

echo $SECRET_KEY | fold -w1
