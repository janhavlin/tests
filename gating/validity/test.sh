#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
. /usr/share/beakerlib/beakerlib.sh || exit 1

# workaround: https://github.com/psss/tmt/issues/971
GATING_YAML=${GATING_YAML:-../../../../../tree/gating.yaml}
GREENWAVE_URL=${GREENWAVE_URL:-https://greewave.fedoraproject.org}

rlJournalStart
    rlPhaseStartTest
        # Check for `gating.yaml` presence
        test -e $GATING_YAML || rlDie "gating.yaml not found"

        # Validate content via Greenwave validate endpoint
        rlRun -s "curl $CURL_OPTIONS --data-binary @$GATING_YAML -X POST $GREENWAVE_URL/api/v1.0/validate-gating-yaml" 0 \
            "Validate $GATING_YAML via Greenwave"
        cat $rlRun_LOG
        rlAssertGrep "All OK" $rlRun_LOG
    rlPhaseEnd
rlJournalEnd
