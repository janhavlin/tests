#!/bin/bash -e

# default reservation time is 30 minutes
[ -z "$TF_RESERVATION_DURATION" ] && TF_RESERVATION_DURATION=30

timestamp_file="/.reserved-until"
authorized_keys="/root/.ssh/authorized_keys"
reservation_timestamp=$(date -d "+$TF_RESERVATION_DURATION minutes" "+%s")

info() { echo "[+] $*"; }

setup() {
    # check for required parameters
    if [ -z "$TF_RESERVATION_AUTHORIZED_KEYS_BASE64" ]; then
      echo "TF_RESERVATION_AUTHORIZED_KEYS_BASE64 is required"
      exit 1
    fi

    # print reservation time
    info "System will be reserved until: $(date -d "@$reservation_timestamp")"

    # set default timestamp to 30 minutes from now, or use the timestamp provided
    info "Setting up reservation file $timestamp_file"
    echo "$reservation_timestamp" > $timestamp_file

    # update SSH authorized keys
    info "Updating $authorized_keys"
    base64 -d <<< "$TF_RESERVATION_AUTHORIZED_KEYS_BASE64" >> $authorized_keys
    cat $authorized_keys

    # add motd
    cat > /etc/motd <<EOF
Welcome to Testing Farm!

Machine is reserved for $TF_RESERVATION_DURATION minutes.

To extend the reservation, run 'extend-reservation MINUTES'.

To return machine back, run 'return2testingfarm'.

To reboot the machine, use 'tmt-reboot'.
EOF

    # add scripts
    cp extend-reservation return2testingfarm /usr/local/bin
}

if [ "$TMT_REBOOT_COUNT" -eq 0 ]; then
    setup
else
    echo "Machine was rebooted $TMT_REBOOT_COUNT time(s), skipping setup ..."
fi

# wait until machine reservation timestamp expired
until [ "$(date +%s)" -gt "$(cat $timestamp_file || date +%s)" ]; do
    if [ ! -e "$timestamp_file" ]; then
        info "Reservation cancelled on $(date)"
        exit
    fi
    info "Reservation tick: $(date)"
    sleep 60
done

info "Reservation expired on $(date)"
