#!/bin/bash -e

if [ "$TMT_REBOOT_COUNT" -eq 0 ]; then 
    echo "BEFORE REBOOT"
    cat /etc/os-release
    echo "REBOOTING"
    tmt-reboot
elif [ "$TMT_REBOOT_COUNT" -eq 1 ]; then
    echo "AFTER REBOOT"
    cat /etc/os-release
fi

